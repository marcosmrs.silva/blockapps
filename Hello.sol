pragma solidity >=0.4.22 <0.6.0;

contract Hello {
    bytes32 name;
    
    // A funcao abaixo funciona corretamente!
    function stringToBytes32(string memory source) public pure returns (bytes32 result) {
        bytes memory tempEmptyStringTest = bytes(source);
        if (tempEmptyStringTest.length == 0) {
            return 0x0;
        }

        assembly {
            result := mload(add(source, 32))
        }
    }

    
    /* bytes to string */
    function bytesArrayToString(bytes memory _bytes) public pure returns (string memory) {
        return string(_bytes);
    } //

    /* bytes32 (fixed-size array) to bytes (dynamically-sized array) */
    function bytes32ToBytes(bytes32 _bytes32) public pure returns (bytes memory){
        bytes memory bytesArray = new bytes(32);
        for (uint256 i; i < 32; i++) {
            bytesArray[i] = _bytes32[i];
        }
        return bytesArray;
    }//
    
    constructor (string memory _name) public {
        name = stringToBytes32(_name);
    }

    function sayHello(string memory _name) public payable returns (string memory) {
        // O codigo abaixo funciona!
        /*
        bytes memory bytesArray = new bytes(32);
        for (uint256 i; i < 32; i++) {
            bytesArray[i] = name[i];
        }
        return string(bytesArray);
        */
        name = stringToBytes32(_name);
        bytes memory b = bytes32ToBytes(name);
        string memory s = bytesArrayToString(b);
        return s;
    }

}