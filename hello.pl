use strict;
use warnings;
use HTTP::Request::Common;
use LWP::UserAgent;
use JSON;
use JSON::Parse ':all';
use Encode qw(encode_utf8);
use JSON::MaybeXS qw(encode_json);
use Data::Dumper;



my $contract = << 'END_CONTRACT';
contract Hello {
    bytes32 name;

    function stringToBytes32(string memory source) public pure returns (bytes32 result) {
        bytes memory tempEmptyStringTest = bytes(source);
        if (tempEmptyStringTest.length == 0) {
            return 0x0;
        }

        assembly {
            result := mload(add(source, 32))
        }
    }

    
    /* bytes to string */
    function bytesArrayToString(bytes memory _bytes) public pure returns (string memory) {
        return string(_bytes);
    } //

    /* bytes32 (fixed-size array) to bytes (dynamically-sized array) */
    function bytes32ToBytes(bytes32 _bytes32) public pure returns (bytes memory){
        bytes memory bytesArray = new bytes(32);
        for (uint256 i; i < 32; i++) {
            bytesArray[i] = _bytes32[i];
        }
        return bytesArray;
    }//
    
    constructor (string memory _name) public {
        name = stringToBytes32(_name);
    }

    function sayHello(string memory _name) public payable returns (string memory) {
        // O codigo abaixo funciona!
        /*
        bytes memory bytesArray = new bytes(32);
        for (uint256 i; i < 32; i++) {
            bytesArray[i] = name[i];
        }
        return string(bytesArray);
        */
        name = stringToBytes32(_name);
        bytes memory b = bytes32ToBytes(name);
        string memory s = bytesArrayToString(b);
        return s;
    }
}
END_CONTRACT


my $username = "Roberto";
my $password = "Roberto";

#my $contract_address = &deployContract("Marcos", $username, $password, $contract);
#my $hash = &callFunction($username, $password, $contract_address);
#my $status = &getStatusTransaction($hash);
my $status = &getStatusTransaction("ee8dc73927d4661b7cc9e5826cb2ef1fa275ef62e16c3050ca178e40a6524995");






#---------------------------------------------------------------------#
sub getStatusTransaction{
    my $hash = $_[0];
    my $url = "http://localhost/bloc/v2.2/transactions/$hash/result";
    my $header = ['accept' => 'application/json;charset=utf-8','content-type' => 'application/json;charset=utf-8',];
    my $status = "";
    my $req = HTTP::Request->new('GET', $url, $header);
	my $ua = LWP::UserAgent->new;

    my $response = $ua->request( $req );
    if ($response->is_success) {
        my $json = JSON->new;
        print $json->pretty->encode($json->decode($response->content));
        my $hData = parse_json($response->content);
        $status = $hData->{'status'};
        print "Status: $status\n";
        
    } else {
        my $txt = $response->content;
        print $txt, "\n";
        print STDERR $response->status_line, "\n";
    }
    return $status;
}
#---------------------------------------------------------------------#
sub callFunction{
    my $user     = $_[0];
    my $password = $_[1];
    my $contract_address = $_[2];

    my $address  = &getUserAddress($user);
    my $name = "Marcos Roberto Silva";
    my $url = "http://localhost/bloc/v2.2/users/$user/$address/contract/Hello/$contract_address/call";
    my $header = ['accept' => 'application/json;charset=utf-8','content-type' => 'application/json;charset=utf-8',];
    my $data = {args => { _name => $name}, password => $password, method => 'sayHello'};
    my $encoded_data = encode_utf8(encode_json($data));
    print $encoded_data, "\n";
    my $hash = "";

    my $req = HTTP::Request->new('POST', $url, $header, $encoded_data);
	my $ua = LWP::UserAgent->new;

    my $response = $ua->request( $req );
    if ($response->is_success) {
        my $json = JSON->new;
        print $json->pretty->encode($json->decode($response->content));
        my $hData = parse_json($response->content);
        $hash = $hData->{'hash'};
        my $status = $hData->{'status'};
        print "Status: $status\n";
        
    } else {
        my $txt = $response->content;
        print $txt, "\n";
        print STDERR $response->status_line, "\n";
    }
    return $hash;
}
#---------------------------------------------------------------------#
sub deployContract
{
    my $name     = $_[0];
    my $user     = $_[1];
    my $password = $_[2];
    my $contract = $_[3];
    my $address  = &getUserAddress($user);
    my $url = "http://localhost/bloc/v2.2/users/$user/$address/contract?resolve=";
    print "URL: $url\n";
    my $header = ['accept' => 'application/json;charset=utf-8','content-type' => 'application/json;charset=utf-8',];
    my $data = {args => { _name => $name}, password => $password, src => $contract};
    my $contract_address;
    
    my $encoded_data = encode_utf8(encode_json($data));
    print $encoded_data, "\n";

    my $req = HTTP::Request->new('POST', $url, $header, $encoded_data);
	my $ua = LWP::UserAgent->new;

    my $response = $ua->request( $req );
    if ($response->is_success) {
        my $json = JSON->new;
        print $json->pretty->encode($json->decode($response->content));
        my $hData = parse_json($response->content);
        $contract_address = $hData->{'txResult'}{'contractsCreated'};
    } else {
        my $txt = $response->content;
        print $txt, "\n";
        print STDERR $response->status_line, "\n";
    }
    return $contract_address;
}
#---------------------------------------------------------------------#
sub getUserAddress
{  # curl -X GET "http://localhost/bloc/v2.2/users/Rodrigo" -H  "accept: application/json;charset=utf-8"
    my $user = $_[0];
    my $add = "";
    my $url = "http://localhost/bloc/v2.2/users/$user";
    my $header = ['Content-Type' => 'application/json;charset=utf-8'];
    my $req = HTTP::Request->new('GET', $url, $header);
    my $ua = LWP::UserAgent->new;
    my $response = $ua->request( $req );

    if ($response->is_success) {
        my $data = parse_json($response->content);
        #print ref $data, "\n";
        #print Dumper($data);
        #print $data->[0], "\n";

        $add = $data->[0];
        print "Address of $user: " . $add . "\n";
    } else {
        my $txt = $response->content;
        print $txt, "\n";
        print STDERR $response->status_line, "\n";
    }
    return $add;
}
#---------------------------------------------------------------------#

